
"""
curl -L -A "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36" -socks socks4://190.217.68.212:4145 www.twse.com.tw
"""

import asyncio, random
from aiohttp import ClientSession, ClientTimeout
from aiohttp_socks import ProxyConnector
from fake_useragent import UserAgent
ua = UserAgent()

from pyquery import PyQuery as pq

async def getProxys():
    async with ClientSession() as session:
        async with session.get("https://www.socks-proxy.net/") as response:
           return pq(await response.text())("textarea").text().split("\n")[3:-1]


async def verify(socksProxy, timeout=5):
    print(f"start proxy: {socksProxy} ...")

    connector = ProxyConnector.from_url(f'socks4://{socksProxy}')
    timeout = ClientTimeout(total=timeout)

    async with ClientSession(connector=connector) as session:    
        try:
            async with session.get("https://www.twse.com.tw", headers={"User-Agent": ua.random}, timeout=timeout) as response:
                print(f"{socksProxy} ... {response.status}")
                return socksProxy
        except Exception as e:
            print(e)
            return

async def headGen():
    RefererList = [
        "https://www.learncodewithmike.com/",
        "https://www.google.com/",
        "https://www.baidu.com/",
        "https://www.msn.com/zh-tw/",
        "https://www.semrush.com/analytics/backlinks/overview/?q=https%3A%2F%2Fwww.acer-group.com%2Fag%2Fen%2FTW%2Fcontent%2Fir-overview/",
        "https://www.hnfhc.com.tw/HNFHC/?language=en/",
        "https://www.jamicon.com.tw/"


    ]

    return {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7",
        "Referer": random.choice(RefererList),
        "User-Agent": ua.random
    }

async def write(avalids):
    # write to file
    with open ("proxylist.txt", "wb") as fw:
        for result in avalids:
            result += "\n"
            fw.write(result.encode("utf-8"))


async def main():
    proxyList = await getProxys()

    tasks = [ asyncio.create_task(verify(socksProxy)) for socksProxy in proxyList ]
    avalids = await asyncio.gather(*tasks, return_exceptions=True)
    avalids = [ proxy for proxy in avalids if proxy]

    print(avalids)
    await write(avalids)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
